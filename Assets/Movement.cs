using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private Transform myMap;
    [SerializeField]
    private GameObject mapPlane;

    public float rotationSpeed = 20.0f;

    private void Awake()
    {
        //mapPlane = GameObject.CreatePrimitive
    }
    // Start is called before the first frame update
    void Start()
    {
      //  mapPlane = gameObject.GetComponent<GameObject>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float rotationsX = Input.GetAxisRaw("Horizontal"); //* rotationSpeed * Time.deltaTime;
        //float RotatingThingX = Vector3.up * rotationSpeed * Time.deltaTime; //* rotationSpeed * Time.deltaTime;
        float rotationsY = Input.GetAxisRaw("Vertical");// * rotationSpeed * Time.deltaTime;
                                                        //float rotation = rYtationSpeed * Time.deltaTime
                                                        //Rotation movement
        /*  if (Input.GetButton("Horizontal"))
        {
            mapPlane.transform.Rotate(rotationSpeed * Time.deltaTime,0,0);
            //mapPlane.transform.Rotate(0, rotationsY, 0.0f, Space.Self);
            Debug.Log(mapPlane.transform.position);
        }
        if (Input.GetButton("Vertical"))
        {
            mapPlane.transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
            Debug.Log(mapPlane.transform.position);
        }*/
        //Rotation on axis 

        if (Input.GetAxis("Vertical") > 0)
        {
            mapPlane.transform.Rotate(rotationSpeed * Time.deltaTime, 0, 0);
            //mapPlane.transform.Rotate(0, rotationsY, 0.0f, Space.Self);
            Debug.Log(mapPlane.transform.position);
            /*var a = transform.TransformDirection(0, 0, 50); //up
            controller.SimpleMove(a * speed);*/
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            mapPlane.transform.Rotate(-rotationSpeed * Time.deltaTime, 0, 0);
            //mapPlane.transform.Rotate(0, rotationsY, 0.0f, Space.Self);
            Debug.Log(mapPlane.transform.position);
            /*var b = transform.TransformDirection(0, 50, 0); // down
            controller.SimpleMove(b * speed);*/
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            mapPlane.transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
            //mapPlane.transform.Rotate(0, rotationsY, 0.0f, Space.Self);
            Debug.Log(mapPlane.transform.position);
            /*var c = transform.TransformDirection(50, 0, 0); //right
            controller.SimpleMove(c * speed);*/
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            mapPlane.transform.Rotate(0, -rotationSpeed * Time.deltaTime,  0);
            //mapPlane.transform.Rotate(0, rotationsY, 0.0f, Space.Self);
            Debug.Log(mapPlane.transform.position);
            /*  var d = transform.TransformDirection(-50, 0, 0); // left
              controller.SimpleMove(d * speed);*/
        }
    }
}
