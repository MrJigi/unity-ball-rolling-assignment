using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningThing : MonoBehaviour
{
    [SerializeField]
    public GameObject Player;
    [SerializeField]
    public Collision collision;

    public BoxCollider collider ;

    public ParticleSystem particle;
    [SerializeField]
    public Canvas winningScreen;
    // Start is called before the first frame update
    void Start()
    {
        //  collider = (BoxCollider)GetComponent<Collider>();
        // collision = (Collision)GetComponent<Collision>();
        winningScreen.enabled = false;
        particle.Pause();
    }

    // Update is called once per frame
    void Update()
    {
       // OnCollisionDetect();
       // OnCollisionEnter(collider.gameObject.tag);
        //OnCollisionDetect();
        // OnCollisionEnter(collider);
        // OnCollisionEnter(collision);
        //OnCollisionEnter(collision);
    }

    /* void OnCollisionEnter(Collision col)
     {

         if (col.gameObject.CompareTag("Player"))
         {
             Debug.Log("We hit the gold boys");
             particle.Play();
             winningScreen.gameObject.active = true ;
         }
     }*/

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided with name " + collision.gameObject.name);
        Debug.Log("Collided with tag " + collision.gameObject.tag);

        //This works enabling particle system and canvavs on collision with Goal
        if (collision.gameObject.tag == "Goal")
        {
            particle.Play();
            winningScreen.enabled = true;
            //winningScreen.gameObject.SetActive(true);
        }
        /*foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal, Color.white);
        }*/
        if (collision.collider.isTrigger)
        {
            winningScreen.enabled = true ;
            
            // winningScreen.gameObject.SetActive(true);
            particle.Play();
            
        }
        // audioSource.Play();
    }

    /*public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided with "+collision.gameObject.tag);
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Collided");
            particle.Play();
            winningScreen.gameObject.SetActive(true);
        }
    }*/

    void OnCollisionDetect()
    {
        Debug.Log(collider.tag);
        if (collider.gameObject.CompareTag("Player")  )
        {
            Debug.Log("Collided");
            particle.Play();
            winningScreen.gameObject.SetActive(true);
        }
    }
}
